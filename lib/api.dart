import 'dart:async';

import 'package:graphql/client.dart';
import 'prefs_singleton.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Api {
  Api();

  static String _apiUri = 'https://endpoint.getstaxapp.com/v1/graphql';
  final SharedPreferences prefs = PrefsSingleton.prefs;
  Future<GraphQLClient> client() async {

    final HttpLink _httpLink = HttpLink(
        uri: _apiUri,
        headers : { "x-hasura-default-role": "user",
          "x-hasura-user-id": prefs.getString('uid'),
          "Authorization": 'Bearer ' + prefs.getString('accessToken') }

    );

    final AuthLink authLink = AuthLink(
      getToken: () async => 'Bearer ' + prefs.getString('accessToken'),
    );
    final Link link = authLink.concat(_httpLink);

    final GraphQLClient _client = GraphQLClient(
      cache: InMemoryCache(),
      link: link,
    );

    return _client;
  }

  Future<dynamic> query(query) async {
    final HttpLink _httpLink = HttpLink(
      uri: _apiUri,
      headers: {
        "x-hasura-default-role": "user",
        "x-hasura-user-id": prefs.getString('uid'),
        "Authorization": 'Bearer ' + prefs.getString('accessToken')
      }
    );
    
    final AuthLink authLink = AuthLink(
      getToken: () async => 'Bearer ' + prefs.getString('accessToken'),
    );
    final Link link = authLink.concat(_httpLink);

    final GraphQLClient _client = GraphQLClient(
      cache: InMemoryCache(),
      link: link,
    );

    final QueryOptions options = QueryOptions(
      document: query,
      variables: <String, dynamic>{},
      errorPolicy: ErrorPolicy.all,
    );

    final QueryResult result = await _client.query(options);
    if (result.hasErrors) {
      if (result.errors[0].message == 'You need to sign in or sign up before continuing.') {
        prefs.clear();
      }
      return null;
    } else {
      print('--------------');
      print(result.data);
      return await result.data;
    }
  }
}
